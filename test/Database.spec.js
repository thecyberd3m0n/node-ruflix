/**
 * Created by tcd-primaris on 17.04.17.
 */
const Database = require('../app/database/Database'),
    SettingsProvider = require('../app/database/SettingsProvider')
    chai = require('chai'),
    expect = require('chai').expect,
    spies = require('chai-spies');

chai.use(spies);
chai.should();
describe('Database', () => {
    let database;
    beforeEach(() => {
        database = new Database();
    });
    it('declared static Collections DTOs should not be undefined', () => {
        let arr = [];
        for (let i in Database.COLLECTIONS) {
            arr.push(database.getCollection(Database.COLLECTIONS[i]));
        }
        expect(arr.length).to.be.equal(Object.keys(Database.COLLECTIONS).length);
        return arr.should.all.be.not.undefined;
    });

    it('getCollection should have DTO object as property', () => {
        let obj = database.getCollection(Database.COLLECTIONS[Object.keys(Database.COLLECTIONS)[0]]);
        return expect(obj.DTO).to.be.ok;
    });

    it('should resolve after saving changes', () => {
        let promise = database.saveChanges();
        promise.then(() => {
            assert(true);
        });
    });

    it('should resolve default settings in fixture', () => {
        return database.loadFixtures().then(() => {
            let settings = database.getCollection(Database.COLLECTIONS.Settings).DTO.get(1);
            console.log('maxConnections');
            expect(settings.maxConnections).to.be.ok;
            console.log('locale');
            expect(settings.locale).to.be.ok;
        });
    });

    it('settingsprovider should be able to get settings', () => {
        return database.loadFixtures().then(() => {
            let maxConnections = SettingsProvider.get('maxConnections');
            expect(maxConnections).to.be.ok;
        });
    });

    it('settingsprovider should be able to set settings', () => {
        return database.loadFixtures().then(() => {
            SettingsProvider.set('testSettings', true);
            expect(SettingsProvider.get('testSettings')).to.be.ok;
        });
    });

});
