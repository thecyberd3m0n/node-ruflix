/**
 * Created by tcd-primaris on 16.04.17.
 */
const RutrackerAPI = require('../app/RutrackerAPIProvider/RutrackerAPIProvider'),
    request = require('request'),
    requestPromise = require('request-promise'),
    chai = require('chai'),
    expect = require('chai').expect,
    spies = require('chai-spies');
chai.use(spies);

describe('RutrackerAPIProvider', () => {
    let rutrackerApi;
    let credentials;
    const EXAMPLE_TOPIC_ID = 5391761;
    const getHttpClient = () => {
        return requestPromise;
    };

    function validateTopic (topic) {
        let properties = ['forum_id',
            'id',
            'info_hash',
            'poster_id',
            'reg_time',
            'seeder_last_seen',
            'seeders',
            'size'];
        return properties.every(x => Object.keys(topic).includes(x));
    }

    beforeEach(() => {
        rutrackerApi = new RutrackerAPI(getHttpClient());
        credentials = {
            username: process.env.RUTRACKER_USER,
            password: process.env.RUTRACKER_PASSWORD
        };
    });

    it('should initialize', () => {
        expect(rutrackerApi).to.be.not.undefined;
    });

    it('by default is not logged in', () => {
        return expect(rutrackerApi.getUser()).to.be.null;
    });

    it('should login properly', () => {
        return rutrackerApi.login(credentials.username, credentials.password)
            .then((user) => {
                expect(user).to.have.property('user');
                expect(user).to.have.property('success');
                expect(rutrackerApi).to.have.property('cookie')
                    .that.is.not.null;
                expect(rutrackerApi.getUser()).to.be.not.null;
            }, (error) => {
                throw new Error(`Login failed ${error.reason}`);
            });
    });

    it('provider should be a singleton', () => {
        let firstInstance = new RutrackerAPI(getHttpClient());
        let secondInstance = new RutrackerAPI(getHttpClient());
        expect(firstInstance).to.be.equal(secondInstance);
    });

    it('should restore session properly', () => {
        return rutrackerApi.login(credentials.username, credentials.password)
            .then(() => {
                rutrackerApi = new RutrackerAPI(getHttpClient());
                expect(rutrackerApi).to.have.property('cookie')
                    .that.is.not.null;
                expect(rutrackerApi.getUser()).to.be.not.null;
            });
    });

    it('should get feed properly', () => {
        return rutrackerApi.getFeed().then(x => validateTopic(x[0]));
    });

    it('should get topic data properly', () => {
        return rutrackerApi.getTopicData([EXAMPLE_TOPIC_ID])
            .then(x => {
                return expect(validateTopic(x[0])).to.be.true &&
                    expect(x[0].id).to.be.equal(EXAMPLE_TOPIC_ID);
            });
    });

    it('should search properly', () => {
        return rutrackerApi.login(credentials.username, credentials.password)
            .then(() => {
                return rutrackerApi.search('Gladiator').then(x => validateTopic(x[0]));
            });
    });

    it('should get tree', () => {
        return rutrackerApi.getTree().then((x) => {
            expect(x).to.be.a('object');
        });
    });

    it('should get torrents from subforum', () => {
        return rutrackerApi.getTorrentsInSubforum(441,20).then((x) => {
            expect(x).to.have.length(40);
            expect(validateTopic(x[0])).to.be.true;
        });
    });
});
