/**
 * Created by tcd-primaris on 22.04.17.
 */
const chai = require('chai'),
    expect = require('chai').expect,
    Utils = require('../app/Utils');

describe('Utils', () => {
    it('should optimize topic properly', () => {
        let exampleResponse = {
            432791: {
                forum_id: 1730,
                id: '432791',
                info_hash: '71439013217D718F9977B4AB1FD36E7D5DC4BED8',
                poster_id: 617708,
                reg_time: 1356210018,
                seeder_last_seen: 1492827661,
                seeders: 6,
                size: 6005089247,
                topic_title: '(Brutal Death) Cannibal Corpse - дискография - 1990-2012, APE/FLAC (image + .cue), lossless',
                tor_status: 8
            }
        };
        let result = Utils.optimizeTopicResponse(exampleResponse);
        expect(result).to.be.a('Array');
    });

});
