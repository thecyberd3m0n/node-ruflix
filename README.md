## TESTING
for passing tests you need to have Rutracker account.
Go to [registration](https://rutracker.org/forum/profile.php?mode=register) and create it, if you have no one.
1. Export environmental variables with credentials:

`export RUTRACKER_USER="your username"`

`export RUTRACKER_PASSWORD="your password"`