const Webtorrent = require('webtorrent'),
    Database = require('../database/Database');

class MediaProvider {
    constructor () {
        this.database = new Database();
        let appSettings = this.database.getCollection(Database.COLLECTIONS.Settings).DTO.get(1);
        let config = {
            maxConns: appSettings.maxConns,
        };
        this.webtorrent = new Webtorrent();
        this.enabled = false;
    };

    enable () {
        let downloaded = this.database.getCollection(Database.COLLECTIONS.Downloaded).DTO;
        let seededTorrentsCount = this.getCollection(Database.COLLECTIONS.Settings).DTO.get(1);
    }
}
module.exports = MediaProvider;