/**
 * Created by tcd-primaris on 15.04.17.
 */
const RutrackerParser = require('rutracker-api'),
    Q = require('q'),
    winston = require('winston'),
    Database = require('../database/Database'),
    feedParser = require('feed-read'),
    Utils = require('../Utils'),
    _ = require('lodash');

const RUTRACKER_API = 'http://api.rutracker.org';
const FEED = 'http://feed.rutracker.org/atom/f/1880.atom';
const TORRENTS_AT_ONCE = 40;
let instance = null;

class RutrackerAPIProvider extends RutrackerParser {
    constructor (client) {
        super();
        if (!instance) {
            instance = this;
        }
        this.user = null;
        this.session = new Database().getCollection('Session');
        this.client = client;
        this.restoreSession();
        return instance;
    }

    /**
     * get cookies from db
     */
    restoreSession () {
        if (this.session.DTO.get(0)) {
            this.cookie = this.session.get(0).cookie;
        }
    };

    /**
     * onLogin handle. Saves cookies and credentials into db
     * @returns {Promise<{T}>}
     */
    onLogin (username, store) {
        let defer = Q.defer();
        if (store) {
            this.session.DTO.insert({
                user: username,
                cookie: this.cookie
            });
            new Database().saveChanges();
        }
        this.user = {username: username};
        winston.log('debug', `save credentials for ${username}`);
        defer.resolve();
        return defer.promise;
    };

    /**
     * Login to rutracker.org
     * @param {string} username
     * @param {string} password
     * @param {boolean} store - if credentials should be stored in DB
     * @returns {Promise<{success: boolean, username: string}>}
     */
    login (username, password, store = true) {
        let defer = Q.defer();
        super.login(username, password);
        this.on('login', function () {
            return this.onLogin(username, store)
                .then(
                    () => defer.resolve({success: true, user: username})
                );
        }.bind(this));
        this.on('login-error', () => defer.reject(
            {
                success: false, reason: 'Bad credentials'
            })
        );
        return defer.promise;
    }

    getUser () {
        return this.user;
    }

    getTopicData (topicIds) {
        return this.client({
            uri:`${RUTRACKER_API}/v1/get_tor_topic_data?by=topic_id&val=${topicIds.join(',')}`,
            method: 'GET',
            json: true,
            gzip: true
        }).then(x => Utils.optimizeTopicResponse(x.result));
    }

    getFeed () {
        let defer = Q.defer();
        feedParser(FEED, (err, response) => {
            let process = response => response.map(Utils.extractIdFromFeedEntry);

            if (!err) {
                let processed = process(response);
                this.getTopicData(processed)
                    .then(x => defer.resolve(x));
            } else {
                defer.reject(err);
            }
        });
        return defer.promise;
    }

    search (query) {
        let defer = Q.defer();
        super.search(query, (x) => {
            let ids = x.map((y) => parseInt(y.id));
            this.getTopicData(ids).then(y => defer.resolve(y));
        });
        return defer.promise;
    }

    getTree () {
        return this.client({
            uri:`${RUTRACKER_API}/v1/static/cat_forum_tree`,
            method: 'GET',
            json: true,
            gzip: true
        });
    }

    getTorrentsInSubforum (subforumId, skip) {
        let process = (response, skip = 0) => {
            let results = [];
            Object.keys(response.result)
                .forEach(x => results.push({id: x, seeders: response.result[x][1]}));
            results = _.chain(results)
                .reject(x => !x.seeders)
                .sortBy(x => -x.seeders)
                .slice(skip, (TORRENTS_AT_ONCE + skip))
                .map(x => x.id);
            return results;
        };
        return this.client({
            uri:`${RUTRACKER_API}/v1/static/pvc/f/${subforumId}`,
            method: 'GET',
            json: true,
            gzip: true
        })
            .then(x => process(x, skip))
            .then(x => this.getTopicData(x));
    }

}
module.exports = RutrackerAPIProvider;
