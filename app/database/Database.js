/**
 * Created by tcd-primaris on 17.04.17.
 */
const loki = require('lokijs'),
    Q = require('q'),
    Fixtures = require('./Fixtures'),
    EventEmitter = require('events');

let instance = null;

class Database extends EventEmitter {
    static get EVENTS () {
        return {
            'READY': 'READY'
        };
    }
    static get LOKICONFIG () {
        return {
            'autosave': false
        };
    }
    static get COLLECTIONS () {
        return {
            'meta': 'meta',
            'Feed': 'Feed',
            'Categories': 'Categories',
            'Forums': 'Forums',
            'SupportedLanguages': 'SupportedLanguages',
            'Session': 'Session',
            'Settings': 'Settings',
            'Downloaded': 'Downloaded'
        };
    }
    constructor () {
        super();
        if (!instance) {
            instance = this;
            this.db = new loki('db.json', Database.LOKICONFIG);
            this.DTOs = [];
            this.createOrLoadCollections();
        }
        return instance;
    }

    loadFixtures () {
        return Fixtures.load(this).then(() => this.emit(Database.EVENTS.READY));
    }

    createOrLoadCollections () {
        for (let i in Database.COLLECTIONS) {
            let collectionName = Database.COLLECTIONS[i];
            let DTO = this.db.getCollection(collectionName) ||
            this.db.addCollection(collectionName, {unique: ['id']});
            this.DTOs.push({name: collectionName, DTO: DTO});
        }
    }

    getCollection (collectionName) {
        let dto = this.DTOs.find(x => x.name === collectionName);
        if (!dto) {
            throw new Error('collection not found');
        }
        return dto;
    }

    saveChanges () {
        let defer = Q.defer();
        this.db.saveDatabase(() => defer.resolve());
        return defer.promise;
    }
}

module.exports = Database;
