const Database = require('./Database');

class SettingsProvider {
    static get (key) {
        let database = new Database();
        let DTO = database.getCollection(Database.COLLECTIONS.Settings).DTO;
        let config = DTO.get(1);
        return config[key];
    }

    static set (key, value) {
        let database = new Database();
        let DTO = database.getCollection(Database.COLLECTIONS.Settings).DTO;
        let config = DTO.get(1);
        config[key] = value;
        DTO.update(config);
        return database.saveChanges();
    }
}

module.exports = SettingsProvider;
