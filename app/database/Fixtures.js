/**
 * Created by tcd-primaris on 25.04.17.
 */
const osLocale = require('os-locale'),
    Q = require('q');
const DEFAULTS = {
    maxConnections: 40
};
class BaseFixture {
    static load (context) {
        let maxConnections;
        let baseFixture = new BaseFixture();
        return baseFixture.prepareData().then((data) => {
            maxConnections = data.maxConnections;
            context.getCollection('Settings').DTO.insert({
                'maxConnections': maxConnections,
                'locale': data.locale,
                'seededTorrentsCount': 5
            });
            context.getCollection('meta').DTO.insert({
                'loaded':true
            });
            return context.saveChanges();
        });
    }

    prepareData () {
        let defer = Q.defer();
        let data = Object.assign({}, DEFAULTS);
        osLocale()
            .then(locale => data.locale = locale)
            .then(() => defer.resolve(data));
        return defer.promise;
    }
}

module.exports = BaseFixture;