/**
 * Created by tcd-primaris on 22.04.17.
 */
/**
 * @enum {{Music: string[], Video: string[]}}
 */
const request = require('request'),
    requestPromise = require('request-promise');

const SUPPORTED_TYPES_MAP = {
    'Music': ['MP3', 'FLAC', 'OGG'],
    'Video': ['HD', 'Rip']
};
class Utils {
    /**
     * @param {Array} substrings
     * @param {string} predicate
     * @return {number} number of index of 1st occurency,
     * -1 if not found
     */
    static containsAny (substrings, predicate) {
        let length = substrings.length;
        while (length--) {
            if (predicate.indexOf(substrings[length]) !== -1) {
                return length;
            }
        }
        return -1;
    }
    /**
     * @param {torrentItem} entry
     * @return {torrentItem}
     */
    static determineType (entry) {
        entry.type = 'unknown';
        for (let i in SUPPORTED_TYPES_MAP) {
            if (SUPPORTED_TYPES_MAP.hasOwnProperty(i)) {
                let testedType = SUPPORTED_TYPES_MAP[i];
                if (Utils.containsAny(testedType, entry.topic_title) > -1) {
                    entry.type = i.toLowerCase();
                    return entry;
                }
            }
        }
        return entry;
    }

    static optimizeTopicResponse (response) {
        let arr = [];
        for (let i in response) {
            if (response.hasOwnProperty(i)) {
                let res = response[i];
                if (res) {
                    res.id = parseInt(i);
                    arr.push(res);
                }
            }
        }
        arr.map(Utils.determineType);
        return arr;
    }

    static getHttpClient () {
        return requestPromise;
    }

    static extractIdFromFeedEntry (entry) {
        return entry.link.match(/\d+$/)[0];
    }
}

module.exports = Utils;